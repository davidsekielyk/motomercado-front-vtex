const labels = document.querySelectorAll(".multicarrusel-item__label");
const tabs = document.querySelectorAll(".multicarrusel-tab");

function toggleShow() {
	const target = this;
	const item = target.classList.contains("multicarrusel-tab")
		? target
		: target.parentElement;
	const group = item.dataset.actabGroup;
	const id = item.dataset.actabId;

	tabs.forEach(function(tab) {
		if (tab.dataset.actabGroup === group) {
			if (tab.dataset.actabId === id) {
				tab.classList.add("multicarrusel-active");
			} else {
				tab.classList.remove("multicarrusel-active");
			}
		}
	});

	labels.forEach(function(label) {
		const tabItem = label.parentElement;

		if (tabItem.dataset.actabGroup === group) {
			if (tabItem.dataset.actabId === id) {
				tabItem.classList.add("multicarrusel-active");
			} else {
				tabItem.classList.remove("multicarrusel-active");
			}
		}
	});
}

labels.forEach(function(label) {
	label.addEventListener("click", toggleShow);
});

tabs.forEach(function(tab) {
	tab.addEventListener("click", toggleShow);
});

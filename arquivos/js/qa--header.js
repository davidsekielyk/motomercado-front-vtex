(()=>{
  navScroll()
  buscador()
  buscadorFocus()
  navMenu()
  subMenu()
  menuTercernivel()
  bodyNoScroll()
})()

// navbar se oculta al bajar - se ve al subir
function navScroll(){
  const body = document.body;
  const scrollUp = "scroll-up";
  const scrollDown = "scroll-down";
  let lastScroll = 0;

  window.addEventListener("scroll", () => {
    const currentScroll = window.pageYOffset;
    if (currentScroll <= 90) {
      body.classList.remove(scrollUp);
      return;
    }
    
    if (currentScroll > lastScroll && !body.classList.contains(scrollDown)) {
      // down
      body.classList.remove(scrollUp);
      body.classList.add(scrollDown);
    } else if (currentScroll < lastScroll && body.classList.contains(scrollDown)) {
      // up
      body.classList.remove(scrollDown);
      body.classList.add(scrollUp);
    }
    lastScroll = currentScroll;
  })
}


function buscador(){

  $("body").on("click",".header-navbar__icons-item-search,.header-top__bg-diag-search", function(){
    $(".header-navbar__header-search,.ui-autocomplete.ui-menu.ui-widget.ui-widget-content.ui-corner-all,.header-navbar__icons-item-search, .bg__shadow-search").addClass("show")
  })

  $("body").on("click",".header-navbar__icons-item-search.show,.bg__shadow-search.show,.header__search-close", function(){
    $(".header-navbar__header-search,.ui-autocomplete.ui-menu.ui-widget.ui-widget-content.ui-corner-all,.header-navbar__icons-item-search, .bg__shadow-search").removeClass("show")
  }) 
  
  $("body").on("click",".header-navbar__icons-item-search,.header-top__bg-diag-search", function(){
    $("header").addClass("block")
  })

  $("body").on("click",".header-navbar__icons-item-search.show,.bg__shadow-search.show,.header__search-close", function(){
    $("header.block").removeClass("block")
  })
}

function buscadorFocus(){

  $(".header-navbar__icons-item-search,.header-top__bg-diag-search").click(function() {
    $(".fulltext-search-box").focus();
  })

}

function navMenu(){

  $("body").on("click",".header-navbar__menu-icon", function(){
    $(".header-navbar__menu,.header-navbar__menu-icon,.bg__shadow,.button__closed").addClass("show")
  })

  $("body").on("click",".header-navbar__menu-icon.show,.bg__shadow.show,.button__closed.show", function(){
    $(".header-navbar__menu,.header-navbar__menu-icon,.bg__shadow,.header-navbar__submenu.one,.button__closed").removeClass("show")
  }) 
}

function subMenu(){
  $("body").on("click",".header-navbar__item.one", function(){
    $(".header-navbar__submenu.one,.header-navbar__submenu-cat-one").addClass("show")
  })

  $("body").on("click",".header-navbar__submenu-cat-one,.header-navbar__menu-icon.show,.bg__shadow.show", function(){
    $(".header-navbar__submenu.one,.header-navbar__submenu-cat-one").removeClass("show")
  })

  $("body").on("click",".header-navbar__item.two", function(){
    $(".header-navbar__submenu.two,.header-navbar__submenu-cat-two").addClass("show")
  })

  $("body").on("click",".header-navbar__submenu-cat-two,.header-navbar__menu-icon.show,.bg__shadow.show", function(){
    $(".header-navbar__submenu.two,.header-navbar__submenu-cat-two").removeClass("show")
  })

  $("body").on("click",".header-navbar__item.three", function(){
    $(".header-navbar__submenu.three,.header-navbar__submenu-cat-three").addClass("show")
  })

  $("body").on("click",".header-navbar__submenu-cat-three,.header-navbar__menu-icon.show,.bg__shadow.show", function(){
    $(".header-navbar__submenu.three,.header-navbar__submenu-cat-three").removeClass("show")
  })

  $("body").on("click",".header-navbar__item.four", function(){
    $(".header-navbar__submenu.four,.header-navbar__submenu-cat-four").addClass("show")
  })

  $("body").on("click",".header-navbar__submenu-cat-four,.header-navbar__menu-icon.show,.bg__shadow.show", function(){
    $(".header-navbar__submenu.four,.header-navbar__submenu-cat-four").removeClass("show")
  })

  $("body").on("click",".header-navbar__item.five", function(){
    $(".header-navbar__submenu.five,.header-navbar__submenu-cat-five").addClass("show")
  })

  $("body").on("click",".header-navbar__submenu-cat-five,.header-navbar__menu-icon.show,.bg__shadow.show", function(){
    $(".header-navbar__submenu.five,.header-navbar__submenu-cat-five").removeClass("show")
  })

  $("body").on("click",".header-navbar__item.six", function(){
    $(".header-navbar__submenu.six,.header-navbar__submenu-cat-six").addClass("show")
  })

  $("body").on("click",".header-navbar__submenu-cat-six,.header-navbar__menu-icon.show,.bg__shadow.show", function(){
    $(".header-navbar__submenu.six,.header-navbar__submenu-cat-six").removeClass("show")
  })
}

function menuTercernivel(){
  $('ul.header-navbar__menutercernivel').hide();

    $('.header-navbar-submenu__item').click(function(event) {
        event.stopPropagation();
        $('> ul', this).toggle();

    });
  
  $('.header-navbar-submenu__vermas').click(function () {
    $(this).children('ul').slideToggle();
  });  
}

function bodyNoScroll(){
  
  $("body").on("click",".header-navbar__menu-icon,.header-navbar__icons-item-search,.header-top__bg-diag-search", function(){
    $("body").addClass("no_scroll");
  })

  $("body").on("click",".header-navbar__menu-icon.show,.bg__shadow-search.show,.bg__shadow.show,.header__search-close,.button__closed.show", function(){
    $("body").removeClass("no_scroll");
  })

}

function verMas(){
  $("header").on("click",".header-navbar-submenu__vermas", function(){
    $(this).next("ul.header_navbar-submenu__oculto").addClass("show");
  })
}

/* function menus(){

  if($(window).width() <= 1024){

    $(document).ready(function(){
        $('.header-navbar__item a').on("click", function(e){
          $(this).next('ul').addClass("show")
          e.stopPropagation();
          e.preventDefault();
        });
      });

}} */
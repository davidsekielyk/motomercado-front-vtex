(() => {
    dataBtnAddCart()
    imgCarrusel()
    breadCum()
    makeRequest()
    .then( data =>{
        dataProduct = data
        setInfo()
    })
    getOtherPayment()
})()


function dataBtnAddCart(){
    // ID SKU
    $(".product-info .btn-agregar-al-carrito").attr("data-id-sku",skuJson_0.skus[0].sku)
    // NAME PRODUCT
    $(".product-info .btn-agregar-al-carrito").attr("data-name-product",skuJson_0.name)
}

function breadCum(){
    /*Breadcrumb text fix*/
    $('.bread-crumb ul li:first-child a').html('<i class="fas fa-home"></i>');
}

function imgCarrusel() {
    const listImg = $(".thumbs a");
    for (let i = 0; i < listImg.length; i++) {
        $(".gallery-thumbs .swiper-wrapper").append('<div class="swiper-slide"><img src="' + listImg.eq(i).attr("rel") + '"/></div>');
        $(".gallery-prod .swiper-wrapper").append('<div class="swiper-slide" id="'+i+'"><figure itemprop="associatedMedia"><img src="' + listImg.eq(i).attr("zoom") + '" data-magnify-src="' + listImg.eq(i).attr("zoom") + '" data-size="1200x1200"/></figure></div>');
    }
    //$(".swiper-container.gallery-prod").append('<div class="cant-fotos bg-gris3 color-violeta1"><strong>' + listImg.length + ' FOTOS</strong></div>')

    const galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 10,
        slidesPerView: 3,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        breakpoints: {
            1024: {
                direction: 'vertical',
                slidesPerView: 4,
                freeMode: false,
            }
        }
    });
    const galleryTop = new Swiper('.gallery-prod', {
        spaceBetween: 10,
        navigation: {
            nextEl: '.gallery-prod .swiper-button-next',
            prevEl: '.gallery-prod .swiper-button-prev',
        },
        thumbs: {
            swiper: galleryThumbs,
        }
    });

    if ($(window).width() >= 768){
        $(".gallery-prod img").magnify();
    }
    // var $zoom = $('.gallery-prod img').magnify();
    // $zoom.destroy();
    const swiper = new Swiper('.mas-product', {
        // Default parameters
        slidesPerView: 3,
        spaceBetween: 10,
        // Responsive breakpoints
        breakpoints: {
            // when window width is >= 320px
            320: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            // when window width is >= 480px
            480: {
                slidesPerView: 3,
                spaceBetween: 30
            },
            // when window width is >= 640px
            640: {
                slidesPerView: 3,
                spaceBetween: 40
            }
        }
    })
}

function makeRequest(){

    return new Promise((res,rej) => {

        let productId = skuJson_0.productId
        let url = "/api/catalog_system/pub/products/search?fq=productId:"+productId 
        
        $.ajax({
            url: url,
            success: function(data){
                res(data)
            },
            error: function(error){
                console.error(error)
                rej(error)
            }
        })

    })

}

function setInfo(){

    // Codigo EAN
    const ean = dataProduct[0].items[0].ean
    $(".codigo-produto").append('<span>'+ean+'</span>')

    // Nombre del producto
    $(".productName").html(skuJson_0.name)

    // Porcentaje de descuento
    if($(".skuListPrice").length){
        const skuListPrice = parseInt($(".skuListPrice").html().slice(2).replace(".","").replace(",","."));

        var precioLista = parseInt($(".skuListPrice").html().slice(2).replace(".","").replace(",","."));
        $(".skuListPrice").remove();
        $(".valor-de").append('<strong class="skuListPrice">$'+precioLista+'</strong>');
        
        const skuBestPrice = parseFloat($(".skuBestPrice").html().slice(2).replace(".","").replace(",","."));
        const porcentaje = Math.round((skuListPrice - skuBestPrice)/(skuListPrice*0.01));
        $(".descricao-preco").append('<em class="valor-desc"><strong class="skuPerc">'+porcentaje+'% OFF</strong></em>');
        $(".gallery-prod").append('<span class="valor-desc2"><strong class="skuPerc"><b>'+porcentaje+'%<br>OFF</b></strong></span>');
    }

}

function increaseValueProd() {
    let value = parseInt($(".quantity-prod").val(), 10);
    value = isNaN(value) ? 0 : value;
    value++;
    $(".quantity-prod").val(value);
}

function decreaseValueProd() {
    let value = parseInt($(".quantity-prod").val(), 10);
    value = isNaN(value) ? 2 : value;
    value == 1 ? value = 2 : '';
    value--;
    $(".quantity-prod").val(value);
}

function getOtherPayment(){
    const msj = $(".see-other-payment-method-link").attr("onclick");
    const end = msj.indexOf("FormaPagamento")-3;
    const start = msj.indexOf("https");
    const url = msj.substring(start,end);
    $.ajax({
        url: url,
        method:"GET",
        success:function(page){
            setOtherPayment(page);
        },
        error:function(data){
            console.log(data);
        }
    });
}
function setOtherPayment(page){
    (new Promise((res,rej)=>{
        // Copiar tablas que ya estan creadas
        const listTarCred = $(page).find("#ddlCartao option");
        for (let i = 0; i < listTarCred.length; i++) {
            const $this = listTarCred.eq(i);
            const nomTar = $this.html().toLowerCase().replace(/\s/g,"");
            console.log(nomTar)
            $("#content-other-payment .tarjetas-credito .list").append('<div class="tar '+nomTar+'" nomtar="'+nomTar+'"></div>');
    
            $("#content-other-payment").append(
                '<div class="t-op" nomtar="'+nomTar+'">'+
                    '<div class="volver hidden-md hidden-lg">'+
                        '<div class="'+nomTar+'"></div>'+
                    '</div>'+
                    '<table id="'+nomTar+'"></table>'+
                '</div>'
            );
            const valueTar = $this.val();
            $("table#"+nomTar).replaceWith($(page).find("#tbl"+valueTar));
            traducirTablas("#tbl"+valueTar);
        }
    
        // Crear tablas de datos sueltos
        const otherListTarCred = $(page).find(".custom");
        let otherTar = "a";
        for (let k = 0; k < otherListTarCred.length; k++) {
            const $this = otherListTarCred.eq(k);
            const nomTar =  $this.find("#ltlBoletoTextoWrapper").html()
                                .replace(/Ã  vista| vezes sem juros| vezes com juros/g,"")
                                .replace(/\d/g,"")
                                .replace(/\s/g,"")
                                .toLowerCase();
            
            const cuota =   $this.find("#ltlBoletoTextoWrapper").html()
                                .replace(/Ã  vista/g,"un pago")
                                .replace(/vezes sem juros/g,'cuotas <span class="hasntInteres">sin interés</span>')
                                .replace(/vezes com juros/g,"cuotas con interés");

            const valorCuota = $this.find("#ltlPrecoWrapper").html().replace(/R/g,"");

            if(otherTar != nomTar){
                otherTar = nomTar
                // Se agrega la tarjeta a la lista
                $("#content-other-payment .tarjetas-credito .list").append('<div class="tar '+nomTar+'" nomtar="'+nomTar+'"></div>');
    
                // Se agrega el content de la tarjeta
                $("#content-other-payment").append(
                    '<div class="t-op" nomtar="'+nomTar+'">'+
                        '<div class="volver hidden-md hidden-lg">'+
                            '<div class="'+nomTar+'"></div>'+
                        '</div>'+
                        '<table id="'+nomTar+'" class="tbl-payment-system">'+
                            '<tbody>'+
                                '<tr class="even">'+
                                    '<th class="parcelas">NÂº de Cuotas</th>'+
                                    '<th class="valor">Valor</th>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td class="parcelas">'+cuota+'</td>'+
                                    '<td>'+valorCuota+'</td>'+
                                '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div>'
                );
            }
            else{
                $("table#"+nomTar).append(
                    '<tr>'+
                        '<td class="parcelas">'+cuota+'</td>'+
                        '<td>'+valorCuota+'</td>'+
                    '</tr>'
                )
            }

            const sinInteres = cuota.indexOf("sin interés");
            if(sinInteres != -1){
                // Obtener numero de sin interes
                const regex = /(\d+)/g;
                const numSinInteres = cuota.match(regex)[0];
                
                // Agregar msj de cantidad maxima de cuotas sin interes para mobile
                $(".tarjetas-credito .tar[nomtar='"+nomTar+"']").html('<div>'+numSinInteres+' Sin interés</div>');
                
                // Agregar Titulo de sin Interes en icono de la lista de las tarjetas
                if(!$(".tarjetas-credito .tar[nomtar='"+nomTar+"']").hasClass("sinInteres")){
                    $(".tarjetas-credito .tar[nomtar='"+nomTar+"']").addClass("sinInteres");
                }
            }
        }

        // Cargar valor de cuotas en efectivo y debito
        const valorUnPago = $(".skuBestPrice").html();
        $("#content-other-payment .pago-efectivo").append('<span class="valor-un-pago">'+valorUnPago+'</span>');
        $("#content-other-payment .tarjetas-debito").append('<span class="valor-un-pago">'+valorUnPago+'</span>');

        res("ok");
    }))
    .then(()=>{
        // Activar la primer tabla
        if ($(window).width() > 768){
            $("#content-other-payment .tarjetas-credito .tar.visa").addClass("active");
            $("#content-other-payment .t-op[nomtar='visa']").addClass("show");
        }
        showTables();
    })

}
function traducirTablas(table){
    
    // Quitar el R$ de los valores de las cuotas
    const listValCuota = $(table+" td:not(.parcelas)");
    for (let i = 0; i < listValCuota.length; i++) {
        const $this = listValCuota.eq(i);
        const valCuotaPeso = $this.html().replace(/R/g,"");
        $this.html(valCuotaPeso);
    }

    // Traducir texto de cuotas y encontrar la mayor cuota sin interes
    const listCuotaText = $(table+" td.parcelas");
    for (let i = 0; i < listCuotaText.length; i++) {
        const $this = listCuotaText.eq(i);
        const nomtar = $this.parents(".t-op").attr("nomtar");
        const text = $this.html();
        const sinInteres = text.indexOf("vezes sem juros");
        if(sinInteres != -1){
            // Obtener numero de sin interes
            const regex = /(\d+)/g;
            const numSinInteres = text.match(regex)[0];

            // Resaltar msj de cuotas sin interes
            const textSinInteres =    text.replace(/\d/g,"")
                                        .replace(/vezes sem juros/g,"");
            $this.html(textSinInteres+" "+numSinInteres+' cuotas <span>sin interés</span>');

            // Agregar msj de cantidad maxima de cuotas sin interes para mobile
            $(".tarjetas-credito .tar[nomtar='"+nomtar+"']").html('<div>'+numSinInteres+' Sin interés</div>');
            
            // Agregar Titulo de sin Interes en icono de la lista de las tarjetas
            if(!$(".tarjetas-credito .tar[nomtar='"+nomtar+"']").hasClass("sinInteres")){
                $(".tarjetas-credito .tar[nomtar='"+nomtar+"']").addClass("sinInteres");
            }
        }
        else{
            const textTraducido = text.replace(/Ã  vista/g,"un pago")
                                    .replace(/vezes com juros/g,"cuotas con interés");
            $this.html(textTraducido);
        }
    }

    // Traducir encabezados de las tablas
    const listEncabezados = $(".tbl-payment-system th");
    for (let i = 0; i < listEncabezados.length; i++) {
        const $this = listEncabezados.eq(i);
        const titleTraducido =  $this.html()
                                    .replace(/NÂº de Parcelas/g,"NÂ° de Cuotas")
                                    .replace(/Valor de cada parcela/g,"Valor");
        $this.html(titleTraducido);
    }
}
function showTables(){
    let contentTar;
    let nomTar;

    $("#content-other-payment .tarjetas-credito").on("click",".tar",function(){
        nomTar = $(this).attr("nomtar");
        $(".tar").removeClass("active"); $(this).addClass("active");
        contentTar = $("#content-other-payment .t-op[nomtar='"+nomTar+"']");
        $("#content-other-payment .t-op").removeClass("show"); contentTar.addClass("show");
    });

    $("#content-other-payment").on("click",".t-op .volver",function(){
        $(this).parent().removeClass("show")
    });
}
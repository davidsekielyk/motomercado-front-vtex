(()=>{  
  sliderPrincipal();
  carruselItems();
  carruselSmall();
  carruselMultimedia();
  setCarruselProd();
 
 
})()


function banInjection(contentEspecifico){
const listaBan = $(contentEspecifico).find(".box-banner")
for (let i = 0; i < listaBan.length; i++) {
    let $this = listaBan.eq(i)
    contentEspecifico.find(".swiper-wrapper").append(
        '<div class="swiper-slide">'+$this.html()+'</div>'  // Agregar cada ban al slide
    );
    // $this.remove();
}
}
function sliderPrincipal(){
if($(".slide-banner-slider").length){
    let cantSlide = $(".slide-banner-slider").length
    for (let i = 0; i < cantSlide; i++) {
        const contentEspecifico = $(".slide-banner-slider").eq(i);
        banInjection(contentEspecifico)
    }
    // Como todo es sincronico se puede ejecutar el swiper al finalizar todo
    const swiperSlide = new Swiper('.slide-banner-slider .swiper-container',{
        loop: true,
        autoplay: {
          delay: 6000,
          disableOnInteraction: false,
      },
        pagination: {
            el: '.slide-banner-slider  .swiper-pagination',
            dynamicBullets: false,
        },
    })
}
}



function carruselItems(){
  if($(".items-section").length){
      const swiperItems = new Swiper('.items', {
          slidesPerView: 'auto',
          autoplay: false,
          loop: false,
          spaceBetween: 10,
          navigation: {
            nextEl: null,
            prevEl: null,
          },
          breakpoints: {
            768: {
              slidesPerView: 'auto',
              spaceBetween: 10,
              autoplay: false,
              navigation: {
                  nextEl: null,
                  prevEl: null,
                }
            },
        
          }
        });

  }
  
  
  }

function carruselSmall(){
    if($(".slide-banner-small").length){
        let cantSlide = $(".slide-banner-small").length
        for (let i = 0; i < cantSlide; i++) {
            const contentEspecifico = $(".slide-banner-small").eq(i);
            banInjection(contentEspecifico)
        }
        // Como todo es sincronico se puede ejecutar el swiper al finalizar todo
        const swiperCategorias = new Swiper('.slide-banner-small .swiper-container',{
          slidesPerView: 'auto',
          spaceBetween: 10,
          navigation: {
            nextEl: null,
            prevEl: null,
          },
          breakpoints: {
            768: {
              slidesPerView: 6,
              spaceBetween: 15,
              autoplay: false,
              navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
              }
            },
        
          }
        })
    }
  }


function carruselMultimedia(){
const swiperMultimedia = $(".carrusel-multimedia .swiper-container");
const $bottomSlide = null; 
const $bottomSlideContent = null; 

const mySwiper = new Swiper(".carrusel-multimedia .swiper-container", {
  spaceBetween: 1,
  slidesPerView: 2,
  centeredSlides: true,
  roundLengths: true,
  loop: true,
  loopAdditionalSlides: 30,
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev"
  },
  breakpoints: {
    768: {
      slidesPerView: 3,

    }
  }});

};



function setCarruselProd(){
const carruselProd = $(".carrusel-prod")
if(carruselProd.length){
    const cantCarruseles = carruselProd.length;
    for (let i = 0; i < cantCarruseles; i++) {
        const carrusel = carruselProd.eq(i)
        carruselInjection(carrusel);
        carruselType(carrusel);
        carruselImg(carrusel);
    }
}
}

function carruselInjection(carrusel){
const listaProduct = carrusel.find(".prateleira .vitrina");

for (let i = 0; i < listaProduct.length; i++) {
    let $this = listaProduct.eq(i).parent();
    carrusel.find(".swiper-wrapper").append(
        '<div class="swiper-slide">'+$this.html()+'</div>'  // Agregar cada producto al carrusel
    );
}

}

function carruselType(carrusel){

    if(carrusel.hasClass("type-1")){
        new Swiper('.carrusel-prod.type-1 .swiper-container',{
            spaceBetween: 20,
            slidesPerView: 'auto',
            loop: false,
            freeMode: true,
            navigation: {
                nextEl: '.carrusel-prod .swiper-button-next',
                prevEl: '.carrusel-prod .swiper-button-prev',
            },
            // autoplay: {
            //     delay: 3200,
            //     disableOnInteraction: false,
            // },
            breakpoints: {
                768: {
                    slidesPerView: 5.5,
                    loop: true,
                    navigation: {
                        nextEl: '.carrusel-prod .swiper-button-next',
                        prevEl: '.carrusel-prod .swiper-button-prev',
                    },
                },
            }
        });
    }
    else if(carrusel.hasClass("type-estandar")){
        new Swiper('.carrusel-prod.type-estandar .swiper-container',{
            spaceBetween: 20,
            slidesPerView: 'auto',
            loop: false,
            freeMode: true,
            navigation: {
                nextEl: '.carrusel-prod .swiper-button-next',
                prevEl: '.carrusel-prod .swiper-button-prev',
            },
            // autoplay: {
            //     delay: 3200,
            //     disableOnInteraction: false,
            // },
            breakpoints: {
                768: {
                    slidesPerView: 6.5,
                    loop: true,
                    navigation: {
                        nextEl: '.carrusel-prod .swiper-button-next',
                        prevEl: '.carrusel-prod .swiper-button-prev',
                    },
                },
            }
        });
    }
}

function carruselImg(carrusel){
console.log(carrusel)
console.log(carrusel.find(".box-banner"))
const boxBanner = carrusel.find(".box-banner")
console.log(boxBanner)
carrusel.find(".redirect").html(boxBanner)
}